$(function() {


$('.main-screen__btn').click(function() {
  $('.main-screen').addClass('slideOutLeft');
  $('.main-screen').removeClass('slideInRight');
})

$('.button-in-home').click(function() {
  $('.main-screen').addClass('slideInRight');

  setTimeout(function() {
    $('.main-screen').removeClass('slideOutLeft');
  }, 1000)
})



$('.content__arrow').click(function(e) {
  e.stopPropagation();

  $('.button-in-home').delay(1000).fadeIn(300);
  $('.content__item.fixed-animated .content-modal, .nav-icon').removeClass('show');
  $(this).removeClass('show');
  $('.fixed-animated .content__text').removeClass('show');
  $('.content__item.fixed-animated').removeClass('fixed-animated');
  $('.content__item').removeClass('hidden');

  $('.nav, .nav__cross').removeClass('show');
})





$('.content__item').click(function(e) {
  checkSetCurrentItem();

  var modalContent = $(this).attr('data-modal');

  if (!$(this).hasClass('fixed-animated')) {
    $('.content__item').addClass('hidden');
    $('.button-in-home').fadeOut(300);
    $(e.target).removeClass('hidden').addClass('fixed-animated');
    $('.content__arrow, .nav-icon').addClass('show');
    $(modalContent).addClass('show');
    $('.fixed-animated .content__text').addClass('show');
  }
})


/////////////////////////////////
///////{ Navigation }////////////
/////////////////////////////////


//open sidebar
$('.nav-icon').click(function() {
  $('.nav, .nav__cross').addClass('show');
})

//close sidebar
$('.nav__cross').click(function() {
  $('.nav, .nav__cross').removeClass('show');
})


//swicth screen

$('.nav__item').click(function() {
  checkSetCurrentItem();

  $('.nav, .nav__cross').removeClass('show');
  var screen = '#' + $(this).attr('data-item-screen');


  $('.content__item.fixed-animated').addClass('hidden');


  $('.content__item.fixed-animated .content-modal').removeClass('show')

  $('.content__item .content__text').removeClass('show');

  setTimeout(function(){
    $('.content__item.fixed-animated.hidden').removeClass('fixed-animated');
    $('.content__item.fixed-animated .content__text').addClass('show');
  },1000)



  $(screen).addClass('fixed-animated').removeClass('hidden').find('.content-modal').addClass('show');



})



function checkSetCurrentItem() {
  setTimeout(function() {
    var id = $('.fixed-animated').attr('id');
    $('.nav__item').removeClass('current-item');
    $('.nav__item[data-item-screen="'+ id +'"]').addClass('current-item');
  }, 2000)
}




});//end eeady




